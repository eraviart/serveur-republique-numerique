# serveur-republique-numerique

A static backup of www.republique-numerique.fr

## Install

```bash
git clone https://git.framasoft.org/etalab/serveur-republique-numerique
cd serveur-republique-numerique
npm install
```

## Usage

### Launch the server

```bash
npm run start
```
