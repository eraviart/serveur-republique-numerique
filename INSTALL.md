# Installation & Configuration

## Initial install

```bash
ssh -p 2215 root@storm.april.org

aptitude -t wheezy-backports install nodejs
update-alternatives --install /usr/bin/node nodejs /usr/bin/nodejs 100
cd /tmp
curl https://www.npmjs.com/install.sh | sh
cd /var/www
mkdir .npm
chown www-data. .npm/
touch .babel.json
chown www-data. .babel.json
npm install -g forever
mkdir /var/log/forever

mkdir /var/www/republique-numerique.april.org
chown www-data: /var/www/republique-numerique.april.org/
chmod ug+s /var/www/republique-numerique.april.org/
mkdir /var/log/apache2/republique-numerique.april.org
chown www-data. /var/log/apache2/republique-numerique.april.org

su - www-data
bash
cd /var/www/republique-numerique.april.org/
git clone https://git.framasoft.org/eraviart/serveur-republique-numerique.git
mv serveur-republique-numerique/* .
mv serveur-republique-numerique/.eslintrc .
mv serveur-republique-numerique/.git* .
rmdir serveur-republique-numerique/
npm install
exit
exit
cd /etc/apache2/sites-available/
ln -s /var/www/republique-numerique.april.org/config/apache2.conf republique-numerique.april.org
cd ../sites-enabled/
ln -s ../sites-available/republique-numerique.april.org

ln -s /var/www/republique-numerique.april.org/config/init.sh /etc/init.d/republique-numerique
update-rc.d republique-numerique defaults
```

## Pages update from fetched directory

From `/home/manou/projects/etalab/serveur-republique-numerique`:

```bash
rsync -avz -e 'ssh -p 2215' public_html/ root@storm.april.org:/var/www/republique-numerique.april.org/public_html/

ssh -p 2215 root@storm.april.org
chown -R www-data:www-data /var/www/republique-numerique.april.org/
```


## Launch

```bash
ssh -p 2215 root@storm.april.org
service republique-numerique start
```

Log files are stored in `/var/log/forever`.
