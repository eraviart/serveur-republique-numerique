#!/bin/sh
### BEGIN INIT INFO
# Provides:          republique-numerique
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# X-Interactive:     true
# Required-Start:
# Required-Stop:
# Short-Description: Start/stop republique-numerique.april.org web server
### END INIT INFO

#export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/var/www/republique-numerique.april.org/node_modules/

case "$1" in
  start)
  exec forever -a -p /var/log/forever --sourceDir=/var/www/republique-numerique.april.org --uid "repnum" start index.js
  ;;

  stop)
  exec forever stop --sourceDir=/var/www/republique-numerique.april.org repnum
  ;;
esac

exit 0
