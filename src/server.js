// serveur-republique-numerique -- AA static backup of www.republique-numerique.fr
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Etalab
// https://git.framasoft.org/etalab/serveur-republique-numerique
//
// serveur-republique-numerique is free software; you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// serveur-republique-numerique is distributed in the hope that it will
// be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import express from "express"
import fs from "fs"
import mime from "mime"
import path from "path"
import staticTransformFactory from "connect-static-transform"


const config = {
  host: "republique-numerique.april.org",
  listen: {
    host: null,  // null => Listen to every IPv4 addresses.
    port: 3000,  // null => Listen to config.port by default
  },
  port: 80,
  proxy: true,  // Is this application used behind a trusted proxy?
  siteDir: "public_html",
  title: "Mirroir République numérique",
}
const hostAndPort = [80, 443].includes(config.port) ? config.host.toString() : `${config.host}:${config.port}`

let translationsText = fs.readFileSync(path.join(config.siteDir, "translations.json"), {encoding: "utf-8"})
let translations = JSON.parse(translationsText)

let app = express()
if (config.proxy) {
  app.set("trust proxy", true)
}
app
  .use(function (req, res, next) {
    let urlPath = req.url.split("?")[0]
    let translatedPath = translations[urlPath]
    if (translatedPath) req.url = urlPath = translatedPath
    let extension = path.extname(urlPath)
    if (urlPath.endsWith("/")) {
      req.url = urlPath + "index.html"
    } else if (!extension) {
      extension = urlPath === "/get_api_token" || urlPath.startsWith("/api/") ? ".json" : ".html"
      req.url = urlPath + extension
    }
    next()
  })
  .use(staticTransformFactory({
    match: /.*\.html/,
    root: config.siteDir,
    transform: function (urlPath, text, send) {
      text = text
        // Add April banner.
        .replace(/<body>/, `<body>
<div style="width: 100%; margin: 0px; padding: 4px; text-align: center; background: white;">
  <p>
    Le gouvernement français a organisé en 2015 sur le site
    www.republique-numerique.fr une consultation publique concernant
    l'avant-projet de loi « pour une République numérique ».
  </p>
  <p>
    Le site contient de très nombreuses propositions dont certaines très
    intéressantes et il serait dommage qu'un jour ce site disparaisse (non
    renouvellement de domaine, d'hébergement...) et avec lui toute
    l'intelligence individuelle/collective qu'il contient.
  </p>
  <p>
    Ce site est donc une archive du site www.republique-numerique.fr réalisée par
    l'April. Les positions exposées dans ce site sont celles de leurs
    auteurs et ne rejoignent pas forcément celles de l'April.
  </p>
</div>`)
        // Remove cookie alert.
        .replace(/cookieChoices\.showCookieConsentBar\([\s\S]*?\);/, "")
        // Remove tracking scripts.
        .replace(/<script>\s*\(function\(i,[\s\S]*?<\/script>/gm, "")
        .replace(/<script type="text\/javascript">window\.NREUM[\s\S]*?<\/script>/gm, "")
        .replace(
          /<!--\s*Start of DoubleClick[\s\S]*?DoubleClick Floodlight Tag: Please do not remove -->/gm,
          "")
      send(text, {"Content-Type": mime.lookup(urlPath)})
    },
  }))
  .use(staticTransformFactory({
    match: /.*\.json/,
    root: config.siteDir,
    transform: function (urlPath, text, send) {
      text = text
        .replace(/https:\\\/\\\/www\.republique-numerique\.fr/g, `http:\\/\\/${hostAndPort}`)
        // .replace(/www\.republique-numerique\.fr/g, `${hostAndPort}`)
      send(text, {"Content-Type": mime.lookup(urlPath)})
    },
  }))
  .use(express.static(config.siteDir))

let host = config.listen.host
let port = config.listen.port || config.port
app.listen(port, host)
console.log(`Listening on ${host || "*"}:${port}...`)
