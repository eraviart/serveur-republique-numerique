// serveur-republique-numerique -- AA static backup of www.republique-numerique.fr
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Etalab
// https://git.framasoft.org/etalab/serveur-republique-numerique
//
// serveur-republique-numerique is free software; you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// serveur-republique-numerique is distributed in the hope that it will
// be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


require("babel-polyfill")  //Must be at the top of the entry point to application.
require("babel-core/register")

require("./src/server")
